FROM openjdk:8-jre-alpine

WORKDIR /app

COPY ./build/libs/spark.server-1.0-SNAPSHOT-all.jar /app

EXPOSE 4567

CMD ["java", "-jar", "spark.server-1.0-SNAPSHOT-all.jar"]